Schedule App
===========

The ``schedule`` app allows staff members to create the schedule for the 
conference's presentations, breaks, lunches, etc.

The ```schedule``` app has a number of models that facilitate building the
structured schedule:

  * Schedule: A high level container that maps to each Conference Section.
  * Day: A Day associated with a Schedule.
  * Room: A Room associated with a Schedule. 
  * Slot Kind: A type of Slot associated with a Schedule.
  * Slot: A discrete time period for a Schedule.
  * Slot Room: A mapping of a Room and Slot for a given Schedule.
  * Presentation: A mapping of a Slot to an approved Proposal from the ```proposals``` app.

Schedule Builder Form
---------------------

It can be cumbersome to generate a schedule through the admin. With that in mind,
a generic schedule builder is available via a Schedule's edit view. For instance,
if a Conference site has a Talks Section and Schedule, the form would be
available for Staff at::

/schedule/talks/edit

.. hint::
   Just to reiterate: before this form will work, you must
   create a ``Section`` (``admin/symposion_conference/section/``) with
   the slug ``talks``; and a ``Schedule``
   (``admin/symposion_schedule/schedule``) for that ``Section``


The form consumes a structured CSV file, from which it will build the schedule. 
Sample CSV data is included below::

"date","time_start","time_end","kind"," room "
"01/24/2018","09:00 am","10:10 am","plenary","CB01.05.009"
"01/24/2018","10:10 am","10:45 am","break","CB01.05.009"
"01/24/2018","10:45 am","11:30 am","talk","CB01.05.009"
"01/24/2018","10:45 am","11:30 am","talk","CB06.03.28"
"01/24/2018","10:45 am","11:30 am","talk","CB11.00.405"
"01/24/2018","10:45 am","11:30 am","talk","CB11.00.401"


.. important::
   It is worth noting that this generates the **structure** of
   the schedule. It does not create Presentation objects. This will
   need to be done manually.

.. tip::
   Sessions such as the ``plenary`` and ``break`` which need to be exlusive will need to be flagged this way in the admin (``/admin/symposion_schedule/slot/``)

One can also **delete** an existing schedule via the delete action. This is
irreversible (save for a database restore).
