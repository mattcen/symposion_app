from django.contrib import admin

from .models import PastEvent

admin.site.register(PastEvent)
