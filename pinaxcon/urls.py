from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.contrib.flatpages.views import flatpage

from django.contrib import admin

import symposion.views


urlpatterns = [
    url(r'^saml2/', include('djangosaml2.urls')),
    url(r"^admin/", include(admin.site.urls)),

    url(r"^speaker/", include("symposion.speakers.urls")),
    url(r"^proposals/", include("symposion.proposals.urls")),
    url(r"^reviews/", include("symposion.reviews.urls")),
    url(r"^schedule/", include("symposion.schedule.urls")),
    url(r"^conference/", include("symposion.conference.urls")),

    url(r"^teams/", include("symposion.teams.urls")),
    url(r'^raffle/', include("pinaxcon.raffle.urls")),

    # Required by registrasion
    url(r'^tickets/payments/', include('registripe.urls')),
    url(r'^tickets/', include('registrasion.urls')),
    url(r'^nested_admin/', include('nested_admin.urls')),
    url(r'^checkin/', include('regidesk.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),

    url(r'^dashboard/', symposion.views.dashboard, name="dashboard"),
    url(r'^boardingpass', RedirectView.as_view(pattern_name="regidesk:boardingpass")),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.insert(0, url(r'^__debug__/', include(debug_toolbar.urls)))

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)#