import copy

from django import forms

from pinaxcon.proposals.fields import HelpTextField
from pinaxcon.proposals.models import TalkProposal, TutorialProposal, MiniconfProposal
from pinaxcon.proposals.models import SysAdminProposal, KernelProposal, OpenHardwareProposal
from pinaxcon.proposals.models import GamesProposal, DevDevProposal, ArtTechProposal
from pinaxcon.proposals.models import OpenEdProposal, DocsProposal, SecurityProposal


DEFAULT_FIELDS =  [
    "title",
    "target_audience",
    "abstract",
    "private_abstract",
    "technical_requirements",
    "project",
    "project_url",
    "recording_release",
    "materials_release",
]

TALK_FORMAT_FIELDS = copy.copy(DEFAULT_FIELDS)
TALK_FORMAT_FIELDS.insert(2, "talk_format")

class ProposalForm(forms.ModelForm):

    required_css_class = 'label-required'

    def clean_description(self):
        value = self.cleaned_data["description"]
        if len(value) > 400:
            raise forms.ValidationError(
                u"The description must be less than 400 characters"
            )
        return value


class DocsProposalForm(ProposalForm):

    class Meta:
        model = DocsProposal
        fields = TALK_FORMAT_FIELDS


class TalkProposalForm(ProposalForm):

    class Meta:
        model = TalkProposal
        fields = [
            "title",
            "target_audience",
            "abstract",
            "private_abstract",
            "technical_requirements",
            "project",
            "project_url",
            "video_url",
            "recording_release",
            "materials_release",
        ]


class TutorialProposalForm(ProposalForm):

    class Meta:
        model = TutorialProposal
        fields = [
            "title",
            "target_audience",
            "abstract",
            "private_abstract",
            "technical_requirements",
            "project",
            "project_url",
            "video_url",
            "recording_release",
            "materials_release",
        ]


class MiniconfProposalForm(ProposalForm):

    class Meta:
        model = MiniconfProposal
        fields = [
            "title",
            "abstract",
            "private_abstract",
            "technical_requirements",
            "recording_release",
            "materials_release",
        ]

class SysadminProposalForm(ProposalForm):

    class Meta:
        model = SysAdminProposal
        fields = TALK_FORMAT_FIELDS


class SecurityProposalForm(ProposalForm):

    class Meta:
        model = SecurityProposal
        fields = TALK_FORMAT_FIELDS


class KernelProposalForm(ProposalForm):

    class Meta:
        model = KernelProposal
        fields = TALK_FORMAT_FIELDS


class GamesProposalForm(ProposalForm):

    HELP_TEXT = ("If you have <strong>any</strong> questions please check our "
        "<a href=\"https://gamesandfoss-lcaminiconf.github.io/lca2019/\" ref="
        "\"noreferrer noopener\" target=\"_blank\">website</a> or contact "
        "the games miniconf organisers. We're excited to hear from you! You "
        "can reach us via email <a href=\"mailto:games@lca.lonely.coffee\">"
        "games@lca.lonely.coffee</a> or twitter DM <a href=\"https://twitter"
        ".com/ducky_tape\" ref=\"noreferrer noopener\" target=\"_blank\">"
        "@ducky_tape</a> and <a href=\"https://twitter.com/the_mcjones\" "
        "ref=\"noreferrer noopener\" target=\"_blank\">@the_mcjones</a>.")

    help_field = HelpTextField(text=HELP_TEXT, label='')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['private_abstract'].help_text = ("it would be helpful if "
            "you could please include a <strong>rough</strong> (don't worry, "
            "we know it might change!) Outline of your presentation, e.g. "
            "'0-2mins intro, 2-5mins the problem, 5-10mins things, 10-15mins "
            "stuff 15-20mins solution'.")

    class Meta:
        model = GamesProposal
        fields = ['help_field', ] + TALK_FORMAT_FIELDS


class OpenHardwareProposalForm(ProposalForm):

    class Meta:
        model = OpenHardwareProposal
        fields = TALK_FORMAT_FIELDS


class OpenEdProposalForm(ProposalForm):
    class Meta:
        NO_TARGET_AUDIENCE_FIELDS = copy.copy(TALK_FORMAT_FIELDS)
        del(NO_TARGET_AUDIENCE_FIELDS[1])

        model = OpenEdProposal
        fields = NO_TARGET_AUDIENCE_FIELDS


class DevDevProposalForm(ProposalForm):

    HELP_TEXT = ("The Developer Developer Miniconf is taking <strong> "
        "20-minute</strong> talks only.")

    help_field = HelpTextField(text=HELP_TEXT, label='')

    class Meta:
        model = DevDevProposal
        fields = ['help_field', ] + DEFAULT_FIELDS


class ArtTechProposalForm(ProposalForm):

    class Meta:
        ARTTECH_FIELDS = copy.copy(DEFAULT_FIELDS)
        ARTTECH_FIELDS.remove("target_audience")
        ARTTECH_FIELDS.append("talk_format")
        ARTTECH_FIELDS.append("can_exhibit")
        ARTTECH_FIELDS.append("exhibition_requirements")

        model = ArtTechProposal
        fields = ARTTECH_FIELDS
