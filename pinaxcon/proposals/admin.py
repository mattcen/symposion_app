from django.contrib import admin

from pinaxcon.proposals import models
from symposion.proposals import models as symposion_models
from symposion.reviews.models import ProposalResult

class CategoryAdmin(admin.ModelAdmin):

    class AdditionalSpeakerInline(admin.TabularInline):
        model = symposion_models.AdditionalSpeaker

    class ProposalResultInline(admin.TabularInline):
        model = ProposalResult
        readonly_fields = ["score"]
        fields = ["status"]

    inlines = [
        AdditionalSpeakerInline,
        ProposalResultInline,
    ]


for model in [ models.TalkProposal, models.TutorialProposal,
               models.MiniconfProposal, models.SysAdminProposal,
               models.KernelProposal, models.GamesProposal,
               models.OpenHardwareProposal, models.OpenEdProposal,
               models.DevDevProposal, models.ArtTechProposal,
               models.DocsProposal, models.SecurityProposal ]:
    admin.site.register(model, CategoryAdmin,
                        list_display = [
                            "id",
                            "title",
                            "speaker",
                            "speaker_email",
                            "kind",
                            "target_audience",
                            "status",
                            "cancelled",
                        ],
                        list_filter = [
                            "result__status",
                            "cancelled",
                        ],
    )

