# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-10-24 07:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proposals', '0014_securityproposal'),
    ]

    operations = [
        migrations.AddField(
            model_name='securityproposal',
            name='talk_format',
            field=models.IntegerField(choices=[(1, 'Presentation (40-45 min)'), (2, 'Short Presentation (20-30 min)'), (3, 'Lightning Talk (5-10 min)')], default=1, help_text='Please indicate your preferred talk length in the private abstract field below.'),
            preserve_default=False,
        ),
    ]
