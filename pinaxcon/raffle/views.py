from django.apps import apps
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.http import require_POST

from pinaxcon.raffle.models import Raffle, Prize


def _is_raffle_admin(user):
    group = apps.get_app_config('pinaxcon_raffle').get_admin_group()
    return group in user.groups.all()


@login_required
def raffle_view(request):
    raffles = Raffle.objects.all()
    for raffle in raffles:
        raffle.tickets = list(raffle.get_tickets(user=request.user))

    return render(request, 'raffle.html', {'raffles': raffles})


@login_required
@user_passes_test(_is_raffle_admin)
def draw_raffle_ticket(request, raffle_id=None):
    if request.method == 'POST' and raffle_id is not None:
        Raffle.objects.get(id=raffle_id).draw(user=request.user)
        return HttpResponseRedirect(reverse('raffle-draw'))

    raffles = Raffle.objects.prefetch_related('draws', 'prizes')
    return render(request, 'raffle_draw.html', {'raffles': raffles})


@login_required
@user_passes_test(_is_raffle_admin)
@require_POST
def raffle_redraw(request, redraw_ticket_id):
    prize = Prize.objects.get(winning_ticket=redraw_ticket_id)
    prize.unlock(user=request.user)
    prize.remove_winner(user=request.user)
    prize.raffle.draw(user=request.user)
    return HttpResponseRedirect(reverse('raffle-draw'))

