from django.conf.urls import url
from pinaxcon.raffle import views


urlpatterns = [
    url(r'^tickets/', views.raffle_view),
    url(r'^draw/(?P<raffle_id>[0-9]+)/$', views.draw_raffle_ticket, name="raffle-draw"),
    url(r'^draw/redraw/([0-9]+)/$', views.raffle_redraw, name="raffle-redraw"),
    url(r'^draw/', views.draw_raffle_ticket, name="raffle-draw"),
]