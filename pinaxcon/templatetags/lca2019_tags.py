from django import template
from django.forms import Form
import re


register = template.Library()


@register.filter
def has_required_fields(form):
    for field in form:
        if isinstance(field, Form):
            if has_required_fields(field):
                return True
        if field.field.required:
            return True
    return False


@register.filter
def any_is_void(invoices):
    for invoice in invoices:
        if invoice.is_void:
            return True
    return False


@register.simple_tag
def listlookup(lookup, target):
    try:
        return lookup[target]
    except IndexError:
        return ''


@register.filter
def clean_text(txt):
    # Remove double/triple/+ spaces from `txt` and replace with single space
    return re.sub(r' {2,}' , ' ', txt)

@register.filter
def twitter_handle(txt):
    # Add @ to twitter handle if not present
    return txt if txt.startswith('@') else '@{}'.format(txt)